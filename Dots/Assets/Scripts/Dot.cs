﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dot : MonoBehaviour
{
    SpriteRenderer dot;
    Color[] dotsColors = { Color.red, Color.green, Color.blue, Color.yellow };
    Line line;
    public bool alreadyActive = false;

    void Start()
    {
        dot = GetComponent<SpriteRenderer>();
        dot.color = dotsColors[Random.Range(0, dotsColors.Length)];
        line = FindObjectOfType<Line>();
    }

    private void OnMouseDown()
    {
        line.FirstDot(this, dot.color);
    }

    private void OnMouseEnter()
    {
        line.AddDot(this, dot.color);
    }

    private void OnMouseDrag()
    {
        line.DragLine();
    }
}
