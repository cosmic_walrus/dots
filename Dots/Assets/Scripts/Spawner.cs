﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    GameObject row;
    bool spawn = true;
    int spawnCount = 0;

    void FixedUpdate()
    {
        if (spawn)
        {
            Instantiate(row, transform);
            spawnCount = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        spawn = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        spawn = false;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        spawn = true;
    }
}
