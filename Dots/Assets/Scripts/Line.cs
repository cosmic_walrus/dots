﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Line : MonoBehaviour
{
    [SerializeField]
    private LineRenderer line;
    private LineRenderer currentLine;
    private Vector2 mousePosition;
    public bool clicked = false;
    public bool drag = true;
    public bool moreThenOne = false;
    public Color lineColor;
    Dot[] activeDots = new Dot[36];
    public int activeDotIndex = 0;

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            foreach(Dot dot in activeDots)
            {
                if (dot != null && moreThenOne) Destroy(dot.gameObject);
            }
            Destroy(currentLine);
            clicked = false;
            drag = true;
            activeDotIndex = 0;
            moreThenOne = false;
        }
    }

    public void FirstDot(Dot dot, Color dotColor)
    {
        clicked = true;
        Vector2 cellPos = dot.transform.position;
        currentLine = Instantiate(line, cellPos, Quaternion.identity);
        currentLine.SetPosition(0, cellPos);
        lineColor = dotColor;
        activeDots[activeDotIndex++] = dot;
        dot.alreadyActive = true;
    }

    public void AddDot(Dot dot, Color dotColor)
    {
        if (clicked && !dot.alreadyActive && lineColor == dotColor)
        {
            Vector2 prevDot = activeDots[activeDotIndex-1].transform.position;
            Vector2 cellPos = dot.transform.position;            
            Vector2 next = prevDot - cellPos;
            Debug.Log(next.magnitude);
            if (next.magnitude <= 1.1f)
            {
                currentLine.SetPosition(currentLine.positionCount - 1, cellPos);
                activeDots[activeDotIndex++] = dot;
                moreThenOne = true;
                dot.alreadyActive = true;
                drag = true;
            }
        }
    }

    public void DragLine()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (drag)
        {
            currentLine.positionCount++;
        }        
        currentLine.SetPosition(currentLine.positionCount - 1, mousePosition);
        drag = false;
    }
}
